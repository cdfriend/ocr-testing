﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.Structure;
using ZXing.QrCode;
using ZXing;


namespace OCR_Testing
{
    public partial class UI : Form
    {
        public ImageData imageToTest;
        private Recognizer imgRecognizer;
        private int imgScaleFactor = 1;
        private CircleF areaToTest;
        public UI()
        {
            InitializeComponent();
            imgRecognizer = new Recognizer();
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            fileDialog.ShowDialog();
        }

        private void fileDialog_FileOk(object sender, CancelEventArgs e)
        {
            pathBox.Text = fileDialog.FileName;
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageToTest = new ImageData(0, pathBox.Text);
                imageToTest.Image = new Image<Bgra, Byte>(pathBox.Text);
                resizeForImage(imageToTest.Image);
            }
            catch (Exception ex)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void resizeForImage(Image<Bgra, byte> img)
        { //adjust UI for picture scale and size
            Rectangle screenRes = Screen.PrimaryScreen.Bounds;
            if (img.Bitmap.Width + 150 > screenRes.Width || img.Bitmap.Height + 150 > screenRes.Height)
            { //adjust size of image to fit screen if necessary
                if (img.Bitmap.Width > img.Bitmap.Height)
                {
                    imgScaleFactor = (img.Bitmap.Width / screenRes.Width) + 3;
                }
                else
                {
                    imgScaleFactor = (img.Bitmap.Height / screenRes.Height) + 3;
                }
                img = img.Resize(img.Bitmap.Width / imgScaleFactor, img.Bitmap.Height / imgScaleFactor, Emgu.CV.CvEnum.INTER.CV_INTER_AREA, true);
            }
            //resize form and image box to fit image
            UI.ActiveForm.Width = img.Bitmap.Width + 36;
            UI.ActiveForm.Height = img.Bitmap.Height + 115;
            imgBox.Size = new Size(img.Bitmap.Width, img.Bitmap.Height);
            imgBox.Image = img.ToBitmap();
            //move buttons
            testButton.Location = new Point(UI.ActiveForm.Width - testButton.Width - 24, imgBox.Height + 18);
            loadButton.Location = new Point(testButton.Location.X, testButton.Location.Y + loadButton.Height + 4);
            browseButton.Location = new Point(loadButton.Location.X - browseButton.Width - 4, testButton.Location.Y + browseButton.Height + 4);
            //move text fields
            pathBox.Location = new Point(pathBox.Location.X, testButton.Location.Y + browseButton.Height + 4);
            outputBox.Location = new Point(outputBox.Location.X, testButton.Location.Y);
            outputLabel.Location = new Point(outputLabel.Location.X, testButton.Location.Y);
            //adjust text field sizes
            outputBox.Size = new Size(testButton.Location.X - outputLabel.Location.X - 15, outputBox.Size.Height);
            pathBox.Size = new Size(browseButton.Location.X - pathBox.Location.X - 6, pathBox.Size.Height);
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            resizeForImage(imgRecognizer.detectAnomalies(imageToTest.Image));
        }

        private void imgBox_Click(object sender, EventArgs e)
        {
            MouseEventArgs mouseArgs = (MouseEventArgs)e;
            if (imageToTest != null)
            {
                
            }
        }
    }
}
