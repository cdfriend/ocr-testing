﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.OCR;
using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;

namespace OCR_Testing
{
    class Recognizer
    {
        //CONSTANTS//
        /// <summary> Used to increase contrast between targets and background in the image.</summary>
        private const double GAMMA_CORRECTION = 2.85;
        /// <summary> The value associated with Gaussian blur in anomaly detection.  
        /// This value must be an odd integer or an exception will be thrown by EmguCV. </summary>
        private const int GAUSSIAN_VALUE = 3;
        //HOUGH TRANSFORM
        private const double CANNY_THRESHOLD = 200;
        private const double CANNY_LINKING = 180;
        private const int ACCUM_THRESHOLD = 20;
        //LINE DETECTION
        private const double RHO_RESOLUTION = 5;
        private const double THETA_RESOLUTION = Math.PI / 45;
        private const double MIN_LINE_WIDTH = 1;
        private const double LINE_DETECTION_GAP = 5;
        //CIRCLE DETECTION
        private const double DOWNSAMPLING = 1;
        private const double MIN_CENTER_DIST = 15;
        private const int MIN_RADIUS = 3;
        private const int MAX_RADIUS = 70;
        //TARGET BOX CREATION
        private const int BOX_PADDING = 3;
        private const int MIN_LINE_DISTANCE = 4;

        public Recognizer()
        {
        }

        public String RecognizeQR(Image<Bgra, Byte> img)
        {
            QRCodeDecoder dec = new QRCodeDecoder();
            QRCodeImage qrImage = new QRCodeBitmapImage(img.ToBitmap());
            return dec.decode(qrImage);
        }

        public Image<Bgra, byte> detectAnomalies(Image<Bgra, byte> input)
        {
            input.Save("./Output/inputImg.jpg");
            input._GammaCorrect(GAMMA_CORRECTION);
            input.Save("./Output/gammaCorrected.jpg");
            Image<Gray, byte> inputGray = input.Convert<Gray, byte>().SmoothGaussian(GAUSSIAN_VALUE).PyrDown().PyrUp();
            inputGray.Save("./Output/grayscale.jpg");
            Image<Gray, byte> imgEdges = inputGray.Canny(200, 200);
            imgEdges.Save("./Output/detectededges.jpg");
            LineSegment2D[][] detectedLines;
            CircleF[][] detectedCircles;
            detectedLines = inputGray.HoughLines(CANNY_THRESHOLD, CANNY_LINKING, RHO_RESOLUTION, THETA_RESOLUTION, ACCUM_THRESHOLD, MIN_LINE_WIDTH, LINE_DETECTION_GAP);
            detectedCircles = inputGray.HoughCircles(new Gray(CANNY_THRESHOLD), new Gray(ACCUM_THRESHOLD), DOWNSAMPLING, MIN_CENTER_DIST, MIN_RADIUS, MAX_RADIUS);
            Image<Bgra, byte> output = inputGray.Convert<Bgra, byte>();
            //create an array list for the maximum number of targets to be detected
            ArrayList targets = new ArrayList(detectedLines.Length + detectedCircles.Length);
            //draw each detected line and circle on the image
            Bgra color = new Bgra(0, 0, 255, 255);
            foreach (LineSegment2D[] lineArray in detectedLines)
            {
                foreach (LineSegment2D line in lineArray)
                {
                    output.Draw(line, color, 2);
                }
                color = new Bgra(0, 255, 0, 255);
            }
            foreach (CircleF[] circleArray in detectedCircles)
            {
                foreach (CircleF circle in circleArray)
                {
                    output.Draw(circle, new Bgra(255, 0, 0, 255), 2);
                    //add circles as targets
                    Rectangle r = new Rectangle((int)(circle.Center.X - circle.Radius - BOX_PADDING), 
                        (int)(circle.Center.Y - circle.Radius - BOX_PADDING),
                        (int) (2 * (circle.Radius + BOX_PADDING)), (int) (2 * (circle.Radius + BOX_PADDING)));
                    targets.Add(r);
                }
            }
            Rectangle[] linetargets = groupLines(detectedLines[0]);
            return output;
        }

        private Rectangle[] groupLines(LineSegment2D[] lineArray)
        {

        }
    }
}